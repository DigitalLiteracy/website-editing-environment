# Website Editing Environment

- [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/DigitalLiteracy%2Fwebsite-editing-environment/master?urlpath=rstudio)
- File → Open Project... → Version Control
- `blogdown::serve_site()`
- Tools → Version Control → Commit...
